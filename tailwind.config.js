module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      fontFamily: {
        Montserrat: ["Montserrat", "sans-serif"],
      },
      textColor: {
        "custom-blue": "#0076AA",
        "custom-yellow": "#e2b040",
        "custom-green": "#26ABA9",
      },
      borderColor: { "custom-blue": "#0076AA", "custom-yellow": "#e2b040" },
      minHeight: {
        400: "400px",
      },
      maxHeight: {
        408: "408px",
      },
      lineHeight: {
        0: "0",
      },
      backgroundColor: (theme) => ({
        ...theme("colors"),
        "custom-blue": "#0076AA",
        "custom-yellow": "#e2b040",
        "custom-green": "#E7FFFF",
        "custom-green-dark": "#26ABA9",
      }),
    },
  },
  plugins: [require("@tailwindcss/forms")],
};
