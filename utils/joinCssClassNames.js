export default function joinCssClassNames(...classes) {
  return classes.filter(Boolean).join(" ");
}
