const steps = [
  {
    stepNumber: 1,
    title: "Book Online",
  },
  {
    stepNumber: 2,
    title: "Get Tested",
  },
  {
    stepNumber: 3,
    title: "Get Results",
  },
];

export default function Steps() {
  return (
    <div className="mt-20 lg:max-w-7xl w-5/6 mx-auto">
      <div className="flex flex-col space-y-2 items-center justify-center">
        <h2 className="text-base text-custom-blue font-semibold uppercase">
          how it works
        </h2>
        <h2 className="font-bold text-4xl text-center">As easy as 1-2-3</h2>
      </div>
      <div className="grid grid-cols-1 gap-y-20 lg:grid-cols-3 lg:gap-y-0 lg:gap-x-8 mt-20">
        {steps.map((step) => (
          <div
            key={step.title}
            className="flex flex-col bg-white rounded-2xl shadow-xl"
          >
            <div className="flex-1 relative pt-16 px-6 pb-8 md:px-8">
              <div className="absolute top-0 right-0 left-0 mx-auto font-semibold text-3xl bg-custom-yellow text-white w-14 h-14 flex justify-center items-center rounded-xl shadow-lg transform -translate-y-1/2">
                {step.stepNumber}
              </div>
              <h4 className="text-xl font-bold text-gray-900 text-center">{step.title}</h4>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
