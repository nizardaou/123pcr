/* eslint-disable */

import { SpeakerphoneIcon, XIcon } from "@heroicons/react/outline";
import { useState } from "react";

export default function Banner() {
  const [open, setOpen] = useState(true);
  return (
    open && (
      <div className="bg-custom-yellow font-Montserrat">
        <div className="max-w-7xl mx-auto py-3 px-3 sm:px-6 lg:px-0">
          <div className="flex items-center justify-between flex-wrap">
            <div className="w-0 flex-1 flex items-center">
              <span className="flex p-2 rounded-lg bg-custom-blue">
                <SpeakerphoneIcon
                  className="h-6 w-6 text-white"
                  aria-hidden="true"
                />
              </span>
              <p className="ml-3 font-medium text-white">
                <span>
                  We've activated our Loyalty Program! Collect points and win!
                </span>
              </p>
            </div>
            <div className="order-3 mt-2 flex-shrink-0 w-full sm:order-2 sm:mt-0 sm:w-auto">
              <a
                href="#loyalty-program"
                className="flex items-center justify-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-custom-blue hover:bg-opacity-90"
              >
                Know more
              </a>
            </div>
            <div className="order-2 flex-shrink-0 sm:order-3 sm:ml-3">
              <button
                onClick={() => {
                  setOpen(false);
                }}
                type="button"
                className="-mr-1 flex p-2 rounded-md hover:bg-custom-blue focus:outline-none focus:ring-2 focus:ring-white sm:-mr-2"
              >
                <span className="sr-only">Dismiss</span>
                <XIcon className="h-6 w-6 text-white" aria-hidden="true" />
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  );
}
