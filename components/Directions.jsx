import Image from "next/image";

export default function Directions() {
  return (
    <div id="directions" className="relative text-gray-900 mt-20 mx-auto">
      <main className="lg:relative flex flex-col max-w-screen-2xl mx-auto">
        <div className="mx-auto order-2 lg:order-1 w-5/6 lg:max-w-7xl text-center lg:py-20 lg:text-left">
          <div className="flex col-span-1 flex-col space-y-5 pt-12 sm:pt-16 lg:pt-0 justify-center items-start lg:w-1/3 lg:pr-10 xl:pr-20">
            <h2 className="text-base text-custom-blue font-semibold">
              Need Directions?
            </h2>
            <div className="space-y-2">
              <h1 className="text-4xl text-left text-gray-700 font-bold">
                Find our lab
              </h1>
              <div>
                <p className="text-gray-700 font-bold max-w-2xl text-left">
                  Address:
                  <br />
                  <span className="font-normal">
                    D20-D001, Block D-20 <br /> Dubai Science Park Complex
                    <br /> Science park, 161 <br /> Dubai, UAE
                  </span>
                </p>
                <p className="text-gray-700 font-bold max-w-2xl text-left">
                  Email:{" "}
                  <a
                    href="mailto: covid19@agiomix.com "
                    className="font-normal text-custom-green underline"
                  >
                    covid19@agiomix.com
                  </a>
                </p>
                <p className="text-gray-700 font-bold max-w-2xl text-left">
                  Phone:{" "}
                  <a
                    href="tel:800123727"
                    className="font-normal text-custom-green underline"
                  >
                    800123PCR (800123727)
                  </a>
                </p>
              </div>
            </div>

            <a
              href="https://www.google.com/maps/place/Agiomix+FZ+LLC/@25.0783986,55.1764068,12z/data=!4m5!3m4!1s0x0:0x13ba64f9af6699cf!8m2!3d25.0783986!4d55.2376899"
              target="_blank"
              rel="noopener noreferrer"
              className="bg-custom-blue uppercase text-white rounded-md font-bold px-5 py-3"
            >
              Get Directions
            </a>
          </div>
        </div>
        <div className="relative flex lg:flex-col justify-center order-1 lg:order-2 w-full h-96 sm:h-auto lg:absolute lg:inset-y-0 lg:right-0 lg:w-2/3 mt-0 lg:max-h-408">
          <Image
            height={408}
            width={1024}
            src="/map.jpg"
            alt="123pcr labs location"
            className=" lg:rounded-bl-3xl inset-0 h-full object-cover lg:object-scale-down lg:h-auto max-h-full"
          />
        </div>
      </main>
    </div>
  );
}
