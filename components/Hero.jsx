/* eslint-disable */
import Image from "next/image";

const Hero = () => {
  return (
    <div
      id="home"
      className="relative bg-white text-gray-900 max-w-screen-2xl mx-auto"
    >
      <main className="lg:relative">
        <div className="mx-auto w-5/6 lg:max-w-7xl py-20 text-center lg:text-left">
          <div className="flex flex-col space-y-10 items-center lg:items-start pr-4 lg:w-1/2 sm:pr-8 xl:pr-16">
            <div className="flex flex-col space-y-4">
              <h1 className="text-4xl tracking-tight font-bold sm:text-5xl lg:text-6xl">
                <span className="block xl:inline">Fast and Reliable</span> {""}
                <span className="block xl:inline text-custom-blue">
                  COVID-19 PCR Testing
                </span>
              </h1>

              <p className="max-w-md text-3xl text-gray-500 md:max-w-3xl">
                Starting at AED <span className="text-3xl font-bold">99</span>
                <span className="text-base">.00</span>
              </p>
            </div>
            <a
              href="https://book.123pcr.ae/v2/#book/category/2/count/1/provider/any/"
              className="bg-custom-blue text-white text-sm sm:text-base uppercase rounded-md font-bold px-2 lg:px-5 py-3"
            >
              Beat the queue &gt; Book online
            </a>
            <a
              href="#walk-in"
              className={`text-sm sm:text-base font-bold text-custom-blue hover:opacity-90`}
            >
              Walk-in to our lab without an appointment
              <span aria-hidden="true"> &rarr;</span>
            </a>
          </div>
        </div>
        <div className="relative w-full h-96 lg:absolute lg:inset-y-0 lg:right-0 lg:w-1/2 lg:h-full">
          <Image
            layout="fill"
            src="/Hero.jpg"
            alt="covid testing"
            className="absolute inset-0 w-full object-cover lg:rounded-bl-3xl h-screen"
          />
        </div>
      </main>
    </div>
  );
};

export default Hero;
