import Nav from "./Nav";
import Footer from "./Footer";
import Head from "next/head";

export default function Layout({ children, title, description, ...props }) {
  return (
    <>
      <Head>
        <title>{title}</title>
        <meta name="description" content={description} />
        <meta
          name="facebook-domain-verification"
          content="ztto7wlra0xzroz1jupjwm91sgypv5"
        />
        <link rel="canonical" href="https://123pcr.ae" />
      </Head>
      <Nav />
      <div className="font-Montserrat">{children} </div>
      <Footer />
    </>
  );
}
