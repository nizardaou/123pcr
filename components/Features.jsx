import Image from "next/image";
const features = [
  {
    title: "Fit-to-Fly status ",
    description:
      "Getting your PCR done before travelling? With our lab, you can get your fit-to-fly certificate.",
    src: "/planefeatures.svg",
  },
  {
    title: "Al HOSN app support",
    description:
      "Stay up-to-date on your results in real-time through the Al HOSN app (available for holders of Emirates ID).",
    src: "/shieldfeatures.svg",
  },
  {
    title: "State-of-the-art RT PCR technologies",
    description:
      "We use the latest testing technologies to ensure the most accurate results as fast as possible.",
    src: "/statepcr.png",
  },
  {
    title: "Easy test result access",
    description:
      "No need to visit our facility for your test results. Just check your phone and/or your email to access your results.",
    src: "/qr-codefeatures.svg",
  },
];

export default function Features() {
  return (
    <div className="flex lg:flex-row flex-col space-y-8 lg:space-y-0 mt-20 mx-auto w-5/6 lg:max-w-7xl">
      <h2 className="font-bold text-4xl flex-grow lg:w-1/3">
        A better way to get your PCR test.
      </h2>
      <div className="grid grid-cols-1 gap-y-10 lg:grid-cols-2 lg:gap-y-10 lg:gap-x-8 lg:w-2/3">
        {features.map((feature) => (
          <div
            key={feature.title}
            className="flex flex-col space-y-4 bg-white lg:px-5"
          >
            <div className="font-semibold text-3xl bg-custom-blue text-white w-14 h-14 flex justify-center items-center rounded-xl shadow-lg">
              <Image
                width={30}
                height={30}
                src={feature.src}
                alt={feature.title}
              />
            </div>
            <h4 className="text-xl font-bold text-gray-900">
              {feature.title}
            </h4>
            <p className="mt-4 text-base text-gray-500">
              {feature.description}
            </p>
          </div>
        ))}
      </div>
    </div>
  );
}
