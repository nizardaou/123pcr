/* eslint-disable */
import { Fragment } from "react";
import { Transition, Popover } from "@headlessui/react";

import { MenuIcon, XIcon } from "@heroicons/react/outline";
import Link from "next/link";
import joinCssClassNames from "../utils/joinCssClassNames";
import { useRouter } from "next/router";
import { useEffect } from "react";
import Banner from "./Banner";

const navigations = [
  {
    name: "PCR Tests",
    href: "/#home",
    background: "bg-transparent",
    text: "text-gray-700",
    current: true,
  },
  {
    name: "About",
    href: "/#about",
    background: "bg-transparent",
    text: "text-gray-700",
    current: false,
  },
  {
    name: "FAQ",
    href: "/#faq",
    background: "bg-transparent",
    text: "text-gray-700",
    current: false,
  },
  {
    name: "Walk-Ins",
    href: "/#walk-in",
    background: "bg-transparent",
    text: "text-gray-700",
    current: false,
  },
  {
    name: "Contact Us",
    href: "/#directions",
    background: "bg-transparent",
    text: "text-gray-700",
    current: false,
  },
];

const Nav = () => {
  const router = useRouter();

  useEffect(() => {
    const onHashChangeStart = (url) => {
      console.log(url);
      navigations.forEach((element) => {
        if (url.includes(element.href)) {
          element.current = true;
        } else element.current = false;
      });
    };

    router.events.on("hashChangeStart", onHashChangeStart);

    return () => {
      router.events.off("hashChangeStart", onHashChangeStart);
    };
  }, [router.events]);
  return (
    <>
      <Banner />
      <Popover className=" bg-white shadow sticky top-0 z-50 font-Montserrat">
        <div className="w-5/6 lg:max-w-7xl mx-auto py-3">
          <div className="flex justify-between items-center md:justify-start md:space-x-10">
            <div className="flex items-center justify-start lg:w-0 lg:flex-1">
              <Link href="/#home" passHref>
                <a className="flex flex-col justify-center">
                  <img
                    width={200}
                    height={57}
                    src="/logo.svg"
                    className="hidden lg:block cursor-pointer "
                    alt="123 PCR"
                  />
                  <img
                    width={150}
                    height={42}
                    src="/logo.svg"
                    className="block lg:hidden cursor-pointer "
                    alt="123 PCR"
                  />
                </a>
              </Link>
            </div>

            <div className="-mr-2 -my-2 md:hidden">
              <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none">
                <span className="sr-only">Open menu</span>
                <MenuIcon className="h-6 w-6" aria-hidden="true" />
              </Popover.Button>
            </div>

            <div className="hidden md:flex items-center justify-end md:flex-1 lg:w-0">
              {navigations.map((navigation, index) => (
                <Link key={index} href={navigation.href}>
                  <a
                    className={joinCssClassNames(
                      navigation.current || navigation.name === "Book Now"
                        ? "font-semibold"
                        : "font-normal",
                      `${navigation.background} ${navigation.text} whitespace-nowrap inline-flex items-center justify-center px-4 py-2 text-base rounded-md hover:bg-opacity-90`
                    )}
                  >
                    {navigation.name}
                  </a>
                </Link>
              ))}
              <a
                href="https://book.123pcr.ae/v2/#book/category/2/count/1/provider/any/"
                className={joinCssClassNames(
                  `font-semibold text-custom-yellow whitespace-nowrap inline-flex items-center justify-center px-4 py-2 text-base rounded-md hover:bg-opacity-90`
                )}
              >
                Book Now
              </a>
            </div>
          </div>
        </div>

        <Transition
          as={Fragment}
          enter="duration-200 ease-out"
          enterFrom="opacity-0 scale-95"
          enterTo="opacity-100 scale-100"
          leave="duration-100 ease-in"
          leaveFrom="opacity-100 scale-100"
          leaveTo="opacity-0 scale-95"
        >
          <Popover.Panel
            focus
            className="absolute mx-auto top-0 inset-x-0 z-10 transition w-11/12 transform origin-top-right md:hidden"
          >
            <div className="rounded-b-lg shadow-lg ring-1 ring-black ring-opacity-5 bg-white divide-y-2 divide-gray-50">
              <div className="py-2 w-5/6 mx-auto">
                <div className="flex items-center justify-between">
                  <div>
                    <img
                      width={150}
                      height={42}
                      src="/logo.svg"
                      className="block lg:hidden cursor-pointer "
                      alt="123 PCR"
                    />
                  </div>
                  <div className="-mr-2">
                    <Popover.Button className="bg-white rounded-md p-2 inline-flex items-center justify-center text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none">
                      <span className="sr-only">Close menu</span>
                      <XIcon className="h-6 w-6" aria-hidden="true" />
                    </Popover.Button>
                  </div>
                </div>
              </div>
              <div className="py-6 px-0 space-y-6">
                <div className="flex flex-col">
                  {navigations.map((navigation, index) => (
                    <Popover.Button key={index}>
                      <Link href={navigation.href}>
                        <a
                          className={joinCssClassNames(
                            navigation.current
                              ? "font-semibold"
                              : "font-normal",
                            `${navigation.text} ${navigation.background} whitespace-nowrap inline-flex items-center justify-center px-4 py-2 text-base rounded-md hover:bg-opacity-90`
                          )}
                        >
                          {navigation.name}
                        </a>
                      </Link>
                    </Popover.Button>
                  ))}
                  <Popover.Button>
                    <a
                      href="https://book.123pcr.ae/v2/#book/category/2/count/1/provider/any/"
                      className={joinCssClassNames(
                        "font-semibold",
                        `text-custom-yellow whitespace-nowrap inline-flex items-center justify-center px-4 py-2 text-base rounded-md hover:bg-opacity-90`
                      )}
                    >
                      Book Now
                    </a>
                  </Popover.Button>
                </div>
              </div>
            </div>
          </Popover.Panel>
        </Transition>
      </Popover>
    </>
  );
};

export default Nav;
