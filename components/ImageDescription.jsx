/* eslint-disable */

import joinCssClassNames from "../utils/joinCssClassNames";

export default function ImageDescription({
  item,
  section,
  sectionID,
  containerClasses,
  innerContainerClass1,
  innerContainerClass2,
}) {
  return (
    <div
      className={joinCssClassNames(
        section ? `${containerClasses}` : `mt-0 bg-white max-w-screen-2xl`,
        "text-gray-900  mx-auto "
      )}
    >
      <div
        className={joinCssClassNames(
          section
            ? `py-0 sm:py-16 ${innerContainerClass1} max-w-screen-2xl`
            : "py-12 sm:pb-0 sm:pt-16 mt-0",
          `relative ${innerContainerClass2} text-gray-900 mx-auto`
        )}
      >
        <main
          className={joinCssClassNames(
            section ? "lg:mb-0" : "lg:mb-12",
            "lg:relative flex flex-col"
          )}
        >
          <div
            id={section ? sectionID : ""}
            className={joinCssClassNames(
              section ? "pt-12 pb-12" : "pt-12 lg:pt-0",
              "mx-auto lg:min-h-400 order-2 lg:order-1 flex lg:justify-end w-5/6 lg:max-w-7xl text-center lg:text-left"
            )}
          >
            <div className="flex flex-col lg:w-1/2 lg:pl-10 space-y-5 text-left justify-center items-start">
              {item.subTitle && (
                <h2 className="text-base text-custom-blue font-semibold">
                  {item.subTitle}
                </h2>
              )}
              <div className="space-y-2">
                <h1 className="text-4xl text-gray-700 font-bold">
                  {item.title}
                </h1>
                <p className="text-gray-700 max-w-2xl">{item.description}</p>
              </div>
              {item.buttonText && (
                <a
                  href="https://www.google.com/maps/place/Agiomix+FZ+LLC/@25.0783986,55.1764068,12z/data=!4m5!3m4!1s0x0:0x13ba64f9af6699cf!8m2!3d25.0783986!4d55.2376899"
                  target="_blank"
                  rel="noopener noreferrer"
                  className="bg-custom-blue uppercase text-white rounded-md font-bold px-5 py-3"
                >
                  {item.buttonText}
                </a>
              )}
            </div>
          </div>
          <div className="relative lg:flex lg:flex-col justify-center w-full h-96 sm:h-auto lg:absolute lg:inset-y-0 lg:left-0 lg:w-1/2">
            <img
              height={400}
              width={768}
              src={item.source}
              alt={item.title}
              className="lg:rounded-br-3xl h-full object-cover lg:object-scale-down  lg:h-auto max-h-full"
            />
          </div>
        </main>
      </div>
    </div>
  );
}
