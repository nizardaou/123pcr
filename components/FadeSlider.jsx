/* eslint-disable */

import { Carousel } from "react-responsive-carousel";
import ImageDescription from "./ImageDescription";
import { ChevronLeftIcon, ChevronRightIcon } from "@heroicons/react/outline";
import joinCssClassNames from "../utils/joinCssClassNames";
import { useState, useEffect } from "react";
import { InView, useInView } from "react-intersection-observer";

export default function FadeSlider(props) {
  const [state, setState] = useState({
    currentSlide: 0,
    autoPlay: true,
  });

  const { ref, inView, entry } = useInView({
    /* Optional options */
    threshold: 0.5,
    triggerOnce: true,
  });

  const next = () => {
    setState((state) => ({
      currentSlide: state.currentSlide + 1,
    }));
  };

  const prev = () => {
    setState((state) => ({
      currentSlide: state.currentSlide - 1,
    }));
  };

  const updateCurrentSlide = (index) => {
    const { currentSlide } = state;

    if (currentSlide !== index) {
      setState({
        currentSlide: index,
      });
    }
  };

  useEffect(() => {
    if (inView) next();
  }, [inView]);

  return (
    <div ref={ref} className="relative">
      <div className="lg:max-w-7xl flex justify-between lg:justify-end top-2/3 absolute lg:mb-24 xl:mb-12 lg:bottom-0 lg:top-auto z-10 w-full right-0 left-0 mx-auto">
        <button
          type="button"
          onClick={prev}
          className={joinCssClassNames(
            state.currentSlide !== 0
              ? "flex lg:bg-opacity-100"
              : "hidden lg:flex lg:bg-opacity-80",
            "items-center justify-center h-12 w-12 cursor-pointer bg-none text-white lg:bg-custom-blue lg:border-r-2 lg:border-white rounded-l-lg z-10"
          )}
        >
          <ChevronLeftIcon className="h-6 w-6" />
        </button>

        <button
          type="button"
          onClick={next}
          className={joinCssClassNames(
            state.currentSlide !== 1
              ? "flex ml-auto lg:ml-0 lg:bg-opacity-100"
              : "hidden lg:flex lg:bg-opacity-80",
            "items-center justify-center h-12 w-12 cursor-pointer bg-none  text-white lg:bg-custom-blue lg:border-l-2 lg:border-white rounded-r-lg z-10"
          )}
        >
          <ChevronRightIcon className="h-6 w-6" />
        </button>
      </div>

      <div
        id="about"
        className="mt-20 bg-gray-50 pt-12 lg:pt-16 mx-auto flex flex-col justify-center items-center space-y-8"
      >
        <h1 className="font-bold text-4xl w-1/2 text-center">About 123PCR</h1>{" "}
        <p className="text-base text-center text-gray-700 w-4/6 lg:max-w-7xl">
          123PCR is a quick and reliable COVID testing service by{" "}
          <a
            className="text-custom-green underline"
            target="_blank"
            rel="noopener noreferrer"
            href="https://agiomix.com/"
          >
            {" "}
            Agiomix Labs
          </a>
          , a leading clinical genetics and specialty diagnostics provider with
          a focus on giving the best care possible to patients. Agiomix serves
          patients, healthcare providers and partner laboratories across the
          globe.
        </p>
        <div className="h-28 lg:max-w-screen-2xl w-5/6 flex flex-col justify-center items-center overflow-visible relative">
          <a
            className="absolute top-0 z-10"
            target="_blank"
            rel="noopener noreferrer"
            href="https://agiomix.com/"
          >
            <img src="/agiomix.png" width={396} height={116} alt="agiomix" />
          </a>

          <div className="w-full h-0.5 bg-gray-200" />
        </div>
      </div>

      <Carousel
        className="bg-gray-50"
        selectedItem={state.currentSlide}
        onChange={updateCurrentSlide}
        showStatus={false}
        showArrows={false}
        showThumbs={false}
        showIndicators={false}
        swipeScrollTolerance={100}
        preventMovementUntilSwipeScrollTolerance
        {...props}
      >
        <ImageDescription
          item={{
            source: "/slider2.png",
            title: "Real-Time Updates",
            description:
              "Receive real time updates on Al Hosn App with a valid Emirates ID or UID.",
          }}
        />
        <ImageDescription
          item={{
            source: "/slider1.png",
            title: "World Class Laboratories",
            description: "Agiomix is both CAP and ISO 15189 accredited.",
          }}
        />
      </Carousel>
    </div>
  );
}
