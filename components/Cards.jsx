/* This example requires Tailwind CSS v2.0+ */
import Image from "next/image";
import joinCssClassNames from "../utils/joinCssClassNames";
const services = [
  {
    name: "Home Service Express PCR",
    href: "https://book.123pcr.ae/v2/#book/category/2/service/14/count/1/provider/any/",
    subTitle: "Home Service | 6-8 Hours",
    description: [
      "Reserve a time online and wait for our confirmation call.",
      "Pay online after our confirmation.",
      "Get the swab at your location.",
    ],
    oldPrice: 249,
    newPrice: 199,
    textColor: "text-custom-yellow",
    backgroundColor: "bg-yellow-50",
    bannerBackground: "bg-custom-yellow",
    group: true,
    src: "/group-discount.svg",
    svgText: "50 AED OFF",
  },
  {
    name: "Home Service Standard PCR",
    href: "https://book.123pcr.ae/v2/#book/category/2/service/15/count/1/provider/any/",
    subTitle: "Home Service | 24 Hours",
    description: [
      "Reserve a time online and wait for our confirmation call.",
      "Pay online after our confirmation.",
      "Get the swab at your location.",
    ],
    oldPrice: 199,
    newPrice: 149,
    textColor: "text-custom-yellow",
    backgroundColor: "bg-yellow-50",
    bannerBackground: "bg-custom-yellow",
    group: true,
    src: "/group-discount.svg",
    svgText: "50 AED OFF",
  },
  {
    name: "Express PCR",
    href: "https://book.123pcr.ae/v2/#book/category/2/service/2/count/1/provider/any/",
    subTitle: "In-lab | 6-8 Hours",
    description: [
      "Book and pay online.",
      "Visit our lab and get tested.",
      "Receive your results by SMS and email in 6-8 hours.",
    ],
    oldPrice: 149,
    textColor: "text-custom-blue",
    backgroundColor: "bg-blue-50",
    bannerBackground: "bg-custom-blue",
    group: false,
    src: "/six-hours-colored.svg",
  },
  {
    name: "Standard PCR",
    href: "https://book.123pcr.ae/v2/#book/category/2/service/3/count/1/provider/any/",
    subTitle: "In-lab | 24 Hours",
    description: [
      "Book and pay online.",
      "Visit our lab and get tested.",
      "Receive your results by SMS and email in 24 hours.",
    ],
    oldPrice: 99,
    textColor: "text-custom-blue",
    backgroundColor: "bg-blue-50",
    bannerBackground: "bg-custom-blue",
    group: false,
    src: "/24hblue_icons.svg",
  },
];

export default function Cards() {
  return (
    <div id="packages" className="bg-white mt-32">
      {/* Overlapping cards */}
      <section
        className="w-5/6 lg:max-w-7xl mx-auto relative z-10 pb-0 px-0"
        aria-labelledby="contact-heading"
      >
        <h2 className="sr-only" id="contact-heading">
          Contact us
        </h2>
        <div className="grid grid-cols-1 gap-y-20 lg:grid-cols-2 lg:gap-y-20 lg:gap-x-8">
          {services.map((service) => (
            <div
              key={service.name}
              className="flex flex-col bg-white rounded-2xl shadow-xl border border-gray-200"
            >
              <div className="flex-1 relative pt-20 px-6 pb-8 md:px-8">
                <div
                  className={`absolute top-0 w-24 h-24 flex justify-center items-center ${service.backgroundColor} rounded-xl shadow-lg transform -translate-y-1/2`}
                >
                  <div className="relative">
                    <Image
                      src={service.src}
                      width={service.svgText ? 73.54 : 63.54}
                      height={service.svgText ? 80 : 70}
                      alt="icon"
                    />
                    <span className="absolute block w-full mx-auto right-0 left-0 px-2 top-6 font-bold text-center z-10 text-white text-xs">
                      {service.svgText}
                    </span>
                  </div>
                </div>

                <h3 className="text-xl font-bold text-gray-900">
                  {service.name}
                </h3>
                <h3
                  className={` absolute top-0 right-8 transform -translate-y-1/2 inline-block ${service.bannerBackground} rounded-3xl text-white px-3 lg:px-5 py-1 text-sm lg:text-base font-semibold`}
                >
                  {service.subTitle}
                </h3>
                {service.group && (
                  <p
                    className={joinCssClassNames(
                      service.textColor,
                      "font-semibold"
                    )}
                  >
                    A minimum booking of 4 is required for this service.
                  </p>
                )}
                <ol className="list-decimal list-outside pl-5 mt-4 space-y-1">
                  {service.description.map((item, index) => (
                    <li
                      key={index}
                      className="text-base text-gray-500 list-item"
                    >
                      {item}
                    </li>
                  ))}
                </ol>
              </div>
              <div className="p-6 flex justify-between sm:justify-center items-end rounded-bl-2xl rounded-br-2xl md:px-8">
                <p className="max-w-md leading-5 sm:leading-0 sm:flex-grow text-gray-500 md:max-w-3xl">
                  AED{" "}
                  <span
                    className={joinCssClassNames(
                      service.newPrice
                        ? "line-through text-xl sm:text-2xl decoration-red-600"
                        : "text-2xl sm:text-3xl",
                      "font-bold leading-5 sm:leading-0"
                    )}
                  >
                    {service.oldPrice}
                  </span>
                  {!service.newPrice && ".00"}{" "}
                  {service.newPrice && (
                    <span className="text-2xl sm:text-3xl font-bold leading-5 sm:leading-0">
                      {service.newPrice}
                    </span>
                  )}
                  <span className="text-sm">{service.group && "/person"}</span>
                </p>

                <a
                  href={service.href}
                  className={`text-sm sm:text-base font-bold ${service.textColor} hover:opacity-90`}
                >
                  Book Now<span aria-hidden="true"> &rarr;</span>
                </a>
              </div>
            </div>
          ))}
        </div>
      </section>
    </div>
  );
}
