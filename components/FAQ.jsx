import { Disclosure } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/outline";
import joinCssClassNames from "../utils/joinCssClassNames";
import Link from "next/link";

const faqs = [
  {
    question: "I can’t log in to Al Hosn App.",
    answer:
      "This is usually due to a mobile number registered under the employer who set up your Emirates ID. In this case, please visit: How To Change Mobile Number In Emirates ID 2021 - Gulf Inside for a step-by-step guide to update your registered phone number.",
  },
  {
    question: "I need to amend the details on my Covid report.",
    answer:
      "Kindly send us an email at <a  href='mailto:covid19@agiomix.com'  class='text-custom-green underline' >covid19@agiomix.com</a> with a picture of your covid report and Passport/Emirates ID.",
  },
  {
    question: "Does the result reflect in Al Hosn app? And how soon?",
    answer:
      "Based on the sample processing time you have chosen, the Al Hosn app will reflect in parallel to the report being generated.",
  },
  {
    question: "My Al Hosn app didn’t get updated. Can you please help?",
    answer:
      "Please contact our Customer Care team on <a  href='tel:800123727'  class='text-custom-green underline' >800123PCR (800123727)</a> or email us on <a href='mailto:covid19@agiomix.com'  class='text-custom-green underline'>covid19@agiomix.com</a> to help you with your request.",
  },
  {
    question: "I didn’t get my result, please help.",
    answer:
      "Please contact our Customer Care team on <a  href='tel:800123727'  class='text-custom-green underline' >800123PCR (800123727)</a> or email us on <a href='mailto:covid19@agiomix.com'  class='text-custom-green underline'>covid19@agiomix.com</a> to help you with your request.",
  },
  {
    question: "Is my report fit for travel?",
    answer:
      "Yes, our lab results are approved by the Dubai Health Authority and is ISO Certified.",
  },
  {
    question:
      "I booked online but your team is saying my booking is not visible in your system.",
    answer:
      "We were facing a technical issue which has now been resolved. However, you are always welcome to visit our facility at the time of your appointment.",
  },
  {
    question: "I booked online but I didn’t receive my confirmation.",
    answer:
      "Not to worry, we have all your details. Please contact our Customer Care team on <a  href='tel:800123727'  class='text-custom-green underline' >800123PCR (800123727)</a> to help you with your request.",
  },
  {
    question:
      "I booked and paid online and the payment has been deducted. However, my booking receipt shows that it is not paid yet.",
    answer:
      "Please contact our Customer Care team on <a  href='tel:800123727'  class='text-custom-green underline' >800123PCR (800123727)</a> or email us on <a href='mailto:covid19@agiomix.com'  class='text-custom-green underline'>covid19@agiomix.com</a> to help you with your request.",
  },
  {
    question:
      "I paid online but I want to cancel by booking. How do I cancel and get my refund?",
    answer:
      "You can cancel your booking though the booking portal. Alternatively, you can email us at <a href='mailto:covid19@agiomix.com'  class='text-custom-green underline'>covid19@agiomix.com</a> with your booking receipt, contact details, and name with your cancellation request. The refund will be back into your account within 5 - 10 days.",
  },
  {
    question:
      "I paid online and the booking link came in. However, the time is not mentioned.",
    answer:
      "Not to worry, we have all your details. Please contact our Customer Care team on <a  href='tel:800123727'  class='text-custom-green underline' >800123PCR (800123727)</a> or email us on <a href='mailto:covid19@agiomix.com'  class='text-custom-green underline'>covid19@agiomix.com</a> to help you with your request.",
  },
  {
    question: "Should I take an appointment or make an online booking?",
    answer:
      "Both methods are accepted. Please choose based on what you are comfortable with. However, an online booking is the quickest way.",
  },
  {
    question: "How will I receive my PCR test results?",
    answer:
      "We will send you the results by SMS (only applicable with local UAE Mobile numbers). You may also choose to register your email address to receive your results in your inbox as well.",
  },
  {
    question: "I made a mistake with my booking. How can I correct it?",
    answer:
      "Please contact our Customer Care team on <a  href='tel:800123727'  class='text-custom-green underline' >800123PCR (800123727)</a> or email us on <a href='mailto:covid19@agiomix.com'  class='text-custom-green underline'>covid19@agiomix.com</a> to help you with your request.",
  },
  {
    question: "What are your opening hours?",
    answer: "We are open 24/7.",
  },
  {
    question: "When will I receive my PCR test results?",
    answer:
      "Depending on the processing time you choose, your results will be ready in 6 or 24 hours.",
  },
  {
    question: "What do I need to bring with me to do a PCR test?",
    answer:
      "Please bring your EMIRATES ID or original copy of your PASSPORT to your appointment and have it readily available to show the reception for registration upon your arrival.",
  },
  {
    question: "My report shows Presumptive Positive. What does that mean?",
    answer:
      "Please note that presumptive positive means the patient is either just starting or just finishing from the virus. Please note you must re-do your test after 72 hours noting that this result will not reflect on Al Hosn.",
  },
  {
    question:
      "I booked online, received a confirmation and the payment gone through. However, 10 minutes later I get a notification that my booking is cancelled.",
    answer:
      "We were facing a technical issue which has now been resolved. However, you are always welcome to visit our facility at the time of your appointment.",
  },
  {
    question: "How do I reschedule my booking?",
    answer:
      "Once you log into your account, you will find a reschedule button just under your booking.",
  },
  {
    question: "How do I gain loyalty points?",
    answer:
      "The loyalty program works in a such a way that for every 10 purchases, you get one purchase free depending on the number of points you have collected. For example, if you’ve purchased 1 Standard PCR Services worth AED 99, you will get 10 points and if you’ve purchased 1 Express PCR Services worth AED 149, you will get 15 points.",
  },
  {
    question: "How do I redeem my loyalty points?",
    answer:
      "<ol class='list-decimal space-y-2 list-inside'><li class='list-item'> You can redeem your loyalty points by logging into your account, clicking on the ‘more’ (on the top right of the page) tab to make visible the drop-down selection and clicking on Gift Cards. Here, you’ll need to select the ‘By Points’ option. Please note though that if you do not have enough points, the tab will not be clickable.</li> <li class='list-item'> The issues Gift Cards have a 3-month validity.</li></ol>",
  },
];

const FAQ = ({ show, viewMore, ...props }) => {
  return (
    <div
      id="faq"
      className={joinCssClassNames(
        viewMore ? "mt-20 bg-gray-50" : "mt-0 bg-white"
      )}
    >
      <div className="lg:max-w-7xl w-5/6 mx-auto py-12 px-4 sm:py-16 sm:px-6 lg:px-8">
        <h2 className="text-center text-4xl font-bold text-gray-900">
          Frequently asked questions
        </h2>
        <div className="max-w-3xl mx-auto divide-y-2 divide-gray-200">
          <dl className="mt-6 space-y-2 divide-y divide-gray-200">
            {faqs.map((faq, index) => {
              if (index + 1 <= show)
                return (
                  <Disclosure as="div" key={faq.question} className="pt-4">
                    {({ open }) => (
                      <>
                        <dt className="text-base">
                          <Disclosure.Button className="text-left w-full flex justify-between items-start text-gray-400">
                            <span className="font-medium text-gray-900">
                              {faq.question}
                            </span>
                            <span className="ml-6 h-7 flex items-center">
                              <ChevronDownIcon
                                className={joinCssClassNames(
                                  open ? "-rotate-180" : "rotate-0",
                                  "h-6 w-6 transform"
                                )}
                                aria-hidden="true"
                              />
                            </span>
                          </Disclosure.Button>
                        </dt>
                        <Disclosure.Panel as="dd" className="mt-2 pr-12">
                          <div
                            dangerouslySetInnerHTML={{
                              __html: `${faq.answer}`,
                            }}
                            className="text-sm text-gray-500"
                          ></div>
                        </Disclosure.Panel>
                      </>
                    )}
                  </Disclosure>
                );
            })}
          </dl>
        </div>
        {viewMore && (
          <div className="text-center text-base mt-8 text-gray-900">
            Got another question? Check full FAQ list{" "}
            <Link href="/faq">
              <a className="text-custom-green underline">here</a>
            </Link>
            .
          </div>
        )}
      </div>
    </div>
  );
};

export default FAQ;
