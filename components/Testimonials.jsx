import { Carousel } from "react-responsive-carousel";
import Image from "next/image";

const images = [
  { id: "1", name: "Amin" },
  { id: "2", name: "blacklight" },
  { id: "3", name: "boombo" },
  { id: "4", name: "Crisanto" },
  { id: "5", name: "David" },
  { id: "6", name: "estebanez" },
  { id: "7", name: "fakhruddin" },
  { id: "8", name: "JG_Vlogs" },
  { id: "9", name: "Lois" },
  { id: "10", name: "maryem" },
  { id: "11", name: "mohammed" },
  { id: "12", name: "nehul" },
  { id: "13", name: "prince" },
  { id: "14", name: "samer" },
  { id: "15", name: "Yassin" },
];

export default function Testimonials() {
  return (
    <>
      <div
        id="reviews"
        className="mt-20 mx-auto flex flex-col justify-center items-center space-y-8 w-5/6 lg:w-full"
      >
        <h1 className="font-bold text-4xl text-center">What our customers say about us</h1>
      </div>
      <Carousel
        className="mt-8 hidden md:block"
        infiniteLoop
        centerMode
        centerSlidePercentage={33}
        showArrows
        showStatus={false}
        showIndicators={false}
        useKeyboardArrows
        autoPlay
        stopOnHover
        swipeable
        dynamicHeight
        emulateTouch
        showThumbs={false}
        selectedItem={0}
        interval={2000}
        transitionTime={500}
        swipeScrollTolerance={5}
      >
        {images.map((image) => {
          return (
            <Image
              height={500}
              width={900}
              key={image.id}
              src={`/reviews/${image.name}.png`}
              alt={`Review by ${image.name}`}
            />
          );
        })}
      </Carousel>

      <Carousel
        className="mt-8 md:hidden"
        infiniteLoop
        centerMode
        centerSlidePercentage={100}
        showArrows
        showStatus={false}
        showIndicators={false}
        useKeyboardArrows
        autoPlay
        stopOnHover
        swipeable
        dynamicHeight
        emulateTouch
        showThumbs={false}
        selectedItem={0}
        interval={2000}
        transitionTime={500}
        swipeScrollTolerance={100}
        preventMovementUntilSwipeScrollTolerance
      >
        {images.map((image) => {
          return (
            <Image
              height={500}
              width={900}
              key={image.id}
              src={`/reviews/${image.name}.png`}
              alt={`Review by ${image.name}`}
            />
          );
        })}
      </Carousel>
    </>
  );
}
