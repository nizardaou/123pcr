module.exports = {
  siteUrl: process.env.SITE_URL || "https://123pcr.ae",
  generateRobotsTxt: true,
  robotsTxtOptions:
    process.env.SITE_URL === "https://123pcr.ae"
      ? {
          policies: [
            {
              userAgent: "*",
              allow: "/",
            },
          ],
        }
      : {
          policies: [
            {
              userAgent: "*",
              disallow: "/",
            },
          ],
        },
  // ...other options
};
