import Hero from "../components/Hero";
import Cards from "../components/Cards";
import FadeSlider from "../components/FadeSlider";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import FAQ from "../components/FAQ";
import Directions from "../components/Directions";
import Layout from "../components/Layout";
import Steps from "../components/Steps";
import Features from "../components/Features";
import ImageDescription from "../components/ImageDescription";
import Testimonials from "../components/Testimonials";

export default function Home() {
  return (
    <>
      <style global jsx>
        {`
          html {
            scroll-behavior: smooth;
            scroll-padding-top: 10rem;
          }

          .carousel .slide img {
            width: auto;
          }

          @media only screen and (max-width: 1023px) {
            .carousel .slide img {
              width: 100%;
            }
          }
        `}
      </style>

      <Layout
        title="123PCR By Agiomix Labs | Trusted PCR Testing Facility In Dubai"
        description="123PCR offers fast PCR test with report delivery as early as in 6 hours. Our PCR test price starts at AED 99. Walk-in or book your visit today."
      >
        <Hero />
        <Cards />
        <FadeSlider />
        <Steps />
        <Testimonials />
        <FAQ show={5} viewMore />
        <ImageDescription
          section
          sectionID="loyalty-program"
          containerClasses="mt-0 bg-white"
          innerContainerClass1="mt-0"
          innerContainerClass2="bg-white"
          item={{
            source: "/123loyalty.png",
            subTitle: "LOYALTY PROGRAM",
            title: "Gain points from every swab!",
            description:
              "Join the loyalty program and gain points from every PCR test you undergo at our labs. You can redeem your points for a free test through a gift card or purchase a gift card for your friends or loved ones.",
          }}
        />
        <Features />

        <ImageDescription
          section
          sectionID="walk-in"
          containerClasses="mt-20 bg-gray-50"
          innerContainerClass1="mt-20"
          innerContainerClass2="bg-gray-50"
          item={{
            source: "/walk-in.png",
            subTitle: "24/7 WALK-IN SERVICE",
            title: "We've Got You Covered Around the Clock",
            description:
              "You do not need to be registered to use the walk-in facility at Agiomix Labs. We are operating a 24 hour service, just turn up at our lab with your Emirates ID or Passport. The 24-hour service is AED149.  We accept both cash and credit cards.",
            buttonText: "find our lab",
          }}
        />

        <Directions />
      </Layout>
    </>
  );
}
