import "tailwindcss/tailwind.css";
import { useEffect } from "react";
import Script from "next/script";
import { useRouter } from "next/router";
import { GTM_ID, pageview } from "../lib/gtm";

function MyApp({ Component, pageProps }) {
  const router = useRouter();
  useEffect(() => {
    router.events.on("routeChangeComplete", pageview);
    return () => {
      router.events.off("routeChangeComplete", pageview);
    };
  }, [router.events]);
  return (
    <>
      <Script
        id="gtm"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
        (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer', '${GTM_ID}');
      `,
        }}
      />
      <Script
        id="chatbot"
        strategy="afterInteractive"
        dangerouslySetInnerHTML={{
          __html: `
              (function (f, r) { 
                window['CloudAgentChat'] = f;
                window[f] = window[f] || function () {
                (window[f].s = window[f].s || []).push(arguments)
                },
                scriptElement = document.createElement('script'),
                m = document.getElementsByTagName('script')[0];
                scriptElement.async = 1;
                scriptElement.src = r;
                m.parentNode.insertBefore(scriptElement, m)
            })('ca', 'https://enterprise.getkookoo.com/chatwidget/chat_v3_1.js');
            ca('1078', 'KK6be3c95b5d1a972868d87fab5e8a8b7f', true, undefined, true);`,
        }}
      />
      <Component {...pageProps} />
    </>
  );
}

export default MyApp;
