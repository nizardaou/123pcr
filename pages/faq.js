import FAQ from "../components/FAQ";
import Layout from "../components/Layout";

export default function Faq() {
  return (
    <Layout title="123PCR By Agiomix Labs" description="">
      <FAQ show={22} viewMore={false} />
    </Layout>
  );
}
