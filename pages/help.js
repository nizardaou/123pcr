import { useState } from "react";
import { Switch } from "@headlessui/react";
import joinCssClassNames from "../utils/joinCssClassNames";
import Layout from "../components/Layout";
import * as yup from "yup";
import { Formik } from "formik";
import Modal from "../components/Modal";

const contactSchema = yup.object({
  email: yup
    .string()
    .email("Enter a valid email address")
    .required("An Email is Required"),
  type: yup.string().required("Please select a request type below"),
  firstName: yup
    .string()
    .min(3, "Too Short!")
    .required("First Name is Required"),
  lastName: yup.string().min(3, "Too Short!").required("Last Name is Required"),
  message: yup.string().min(3, "Too Short!").required("Message is Required"),
  phone: yup.string().min(3, "Too Short!").required("Phone is Required"),
  message: yup.string().min(3, "Too Short!").required("Message is Required"),
});

export default function Help() {
  const [agreed, setAgreed] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [open, setOpen] = useState(false);

  function openState(state) {
    setOpen(state);
  }
  return (
    <Layout title="123PCR By Agiomix Labs | Trusted PCR Testing Facility In Dubai">
      <Modal open={open} openState={openState} />
      <div className="bg-white py-16 px-4 overflow-hidden sm:px-6 lg:px-8 lg:py-24">
        <div className="relative max-w-xl mx-auto">
          <svg
            className="absolute left-full transform translate-x-1/2"
            width={404}
            height={404}
            fill="none"
            viewBox="0 0 404 404"
            aria-hidden="true"
          >
            <defs>
              <pattern
                id="85737c0e-0916-41d7-917f-596dc7edfa27"
                x={0}
                y={0}
                width={20}
                height={20}
                patternUnits="userSpaceOnUse"
              >
                <rect
                  x={0}
                  y={0}
                  width={4}
                  height={4}
                  className="text-gray-200"
                  fill="currentColor"
                />
              </pattern>
            </defs>
            <rect
              width={404}
              height={404}
              fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"
            />
          </svg>
          <svg
            className="absolute right-full bottom-0 transform -translate-x-1/2"
            width={404}
            height={404}
            fill="none"
            viewBox="0 0 404 404"
            aria-hidden="true"
          >
            <defs>
              <pattern
                id="85737c0e-0916-41d7-917f-596dc7edfa27"
                x={0}
                y={0}
                width={20}
                height={20}
                patternUnits="userSpaceOnUse"
              >
                <rect
                  x={0}
                  y={0}
                  width={4}
                  height={4}
                  className="text-gray-200"
                  fill="currentColor"
                />
              </pattern>
            </defs>
            <rect
              width={404}
              height={404}
              fill="url(#85737c0e-0916-41d7-917f-596dc7edfa27)"
            />
          </svg>
          <div className="text-center">
            <h1 className="font-bold text-4xl text-center">Contact us</h1>
            <p className="mt-4 text-lg leading-6 text-gray-500">
              Nullam risus blandit ac aliquam justo ipsum. Quam mauris volutpat
              massa dictumst amet. Sapien tortor lacus arcu.
            </p>
          </div>
          <div className="mt-12">
            <Formik
              validationSchema={contactSchema}
              onSubmit={async (values, actions) => {
                try {
                  setSubmitted(true);
                  if (agreed) {
                    setOpen(true);
                  } else {
                    console.log("accept privacy and policy");
                  }
                } catch (err) {}
              }}
              initialValues={{
                email: "",
                firstName: "",
                lastName: "",
                type: "",
                type: "",
                phone: "",
                message: "",
              }}
            >
              {({
                handleSubmit,
                handleChange,
                handleBlur,
                setFieldValue,
                values,
                touched,
                isValid,
                errors,
              }) => (
                <form
                  noValidate
                  onSubmit={handleSubmit}
                  className="grid grid-cols-1 gap-y-6 sm:grid-cols-2 sm:gap-x-8"
                >
                  <div>
                    <label
                      htmlFor="first-name"
                      className="block text-sm font-medium text-gray-700"
                    >
                      First name
                    </label>
                    <div className="mt-1">
                      <input
                        type="text"
                        name="firstName"
                        id="first-name"
                        autoComplete="given-name"
                        className="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                        onChange={handleChange}
                      />
                      {touched.firstName && errors.firstName ? (
                        <span className="text-red-700 mt-2 text-sm">
                          {errors.firstName}
                        </span>
                      ) : null}
                    </div>
                  </div>
                  <div>
                    <label
                      htmlFor="last-name"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Last name
                    </label>
                    <div className="mt-1">
                      <input
                        type="text"
                        name="lastName"
                        id="last-name"
                        autoComplete="family-name"
                        className="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                        onChange={handleChange}
                      />
                      {touched.lastName && errors.lastName ? (
                        <span className="text-red-700 mt-2 text-sm">
                          {errors.lastName}
                        </span>
                      ) : null}
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="type"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Request type
                    </label>
                    <select
                      id="type"
                      name="type"
                      className="mt-1 block w-full pl-3 pr-10 py-3 text-base border-gray-300 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm rounded-md"
                      defaultValue=""
                      onChange={handleChange}
                    >
                      <option value="" disabled>
                        Choose here
                      </option>
                      <option>Update details on report</option>
                      <option>Raise a complaint</option>
                      <option>Request a refund</option>
                      <option>Other</option>
                    </select>

                    {touched.type && errors.type ? (
                      <span className="text-red-700 mt-2 text-sm">
                        {errors.type}
                      </span>
                    ) : null}
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="email"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Email
                    </label>
                    <div className="mt-1">
                      <input
                        id="email"
                        name="email"
                        type="email"
                        autoComplete="email"
                        className="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                        onChange={handleChange}
                      />
                      {touched.email && errors.email ? (
                        <span className="text-red-700 mt-2 text-sm">
                          {errors.email}
                        </span>
                      ) : null}
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="phone-number"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Phone Number
                    </label>
                    <div className="mt-1">
                      <input
                        type="text"
                        name="phone"
                        id="phone-number"
                        autoComplete="tel"
                        className="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border-gray-300 rounded-md"
                        onChange={handleChange}
                      />
                      {touched.phone && errors.phone ? (
                        <span className="text-red-700 mt-2 text-sm">
                          {errors.phone}
                        </span>
                      ) : null}
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <label
                      htmlFor="message"
                      className="block text-sm font-medium text-gray-700"
                    >
                      Message
                    </label>
                    <div className="mt-1">
                      <textarea
                        id="message"
                        name="message"
                        rows={4}
                        className="py-3 px-4 block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 border border-gray-300 rounded-md"
                        defaultValue={""}
                        onChange={handleChange}
                      />

                      {touched.message && errors.message ? (
                        <span className="text-red-700 mt-2 text-sm">
                          {errors.message}
                        </span>
                      ) : null}
                    </div>
                  </div>
                  <div className="sm:col-span-2">
                    <div className="flex items-start">
                      <div className="flex-shrink-0">
                        <Switch
                          checked={agreed}
                          onChange={setAgreed}
                          className={joinCssClassNames(
                            agreed ? "bg-custom-blue" : "bg-gray-200",
                            "relative inline-flex flex-shrink-0 h-6 w-11 border-2 border-transparent rounded-full cursor-pointer transition-colors ease-in-out duration-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                          )}
                        >
                          <span className="sr-only">Agree to policies</span>
                          <span
                            aria-hidden="true"
                            className={joinCssClassNames(
                              agreed ? "translate-x-5" : "translate-x-0",
                              "inline-block h-5 w-5 rounded-full bg-white shadow transform ring-0 transition ease-in-out duration-200"
                            )}
                          />
                        </Switch>
                      </div>
                      <div className="ml-3">
                        <p className="text-base text-gray-500">
                          By selecting this, you agree to the{" "}
                          <a
                            href="#"
                            className="font-medium text-custom-green underline"
                          >
                            Privacy Policy
                          </a>{" "}
                          .
                        </p>
                      </div>
                    </div>
                    {!agreed && submitted && (
                      <span className="text-red-700 mt-2 text-sm">
                        Make sure you agree on our privacy and policy
                      </span>
                    )}
                  </div>
                  <div className="sm:col-span-2">
                    <button
                      type="submit"
                      className="w-full inline-flex items-center justify-center px-6 py-3 border border-transparent rounded-md shadow-sm text-base font-medium text-white bg-custom-blue hover:bg-opacity-90 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                    >
                      Confirm request
                    </button>
                  </div>
                </form>
              )}
            </Formik>
          </div>
        </div>
      </div>
    </Layout>
  );
}
